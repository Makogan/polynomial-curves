import matplotlib.pyplot as plt
import numpy as np

def Poly1(z):
    return z**4/4.0 - 8.0*z**3/3.0 + 17.0*z**2/2 - 10.0*z

cnums = np.arange(300) + 1j * np.arange(300)
cnums = [Poly1(x) for x in cnums]
X = [x.real for x in cnums]
Y = [x.imag for x in cnums]
plt.scatter(X,Y, color='red')
plt.show()