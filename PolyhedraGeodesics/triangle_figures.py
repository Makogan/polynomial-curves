from pyx import *
from pyx.metapost.path import beginknot, endknot, smoothknot, tensioncurve

unit.set(wscale=5)
c = canvas.canvas()

# Triangles
c.stroke(path.line(0, 0, 10, 0) +
         path.line(10, 0, 0, 10) +
         path.line(0, 10, 0, 0), [style.linecap.round])

c.stroke(path.line(0, 0, 10, 0) +
         path.line(10, 0, 10, -10) +
         path.line(10, -10, 0, 0), [style.linecap.round])

# Points
c.stroke(path.line(3, 4, 8, -2), [color.rgb.red])
c.stroke(path.line(3, 4, 3, 0), [style.linestyle.dashed, color.rgb(0.5,0.5,0.5)])
c.stroke(path.line(8, -2, 8, 0), [style.linestyle.dashed, color.rgb(0.5,0.5,0.5)])

# End points
c.fill(path.circle(3, 4, 0.15))
c.fill(path.circle(8, -2, 0.15))
c.text(3, 4.2, r"$E$", [text.halign.boxcenter])
c.text(3, -0.4, r"$E'$", [text.halign.boxcenter])
c.text(8, -2.45, r"$S$", [text.halign.boxcenter])
c.text(8, 0.2, r"$S'$", [text.halign.boxcenter])
c.text(6.315, 0.3, r"$G$", [text.halign.boxcenter])

# Projections
c.fill(path.circle(3, 0, 0.15), [color.rgb(0,0,1)])
c.fill(path.circle(8, 0, 0.15), [color.rgb(0,0,1)])

c.fill(path.circle(6.315, 0, 0.15), [color.rgb(0,0.9,0)])

c.writeEPSfile("path")
c.writePDFfile("path")
c.writeSVGfile("path")
