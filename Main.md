# Interpolating Parametric Curves

Parametric curves are an integral part of many computer graphics applications. In particular, parametric curves created from a set of control points are especially useful to allow users and programs to generate geometry in intuitive ways, making them one of the most fundamental tools in computer modelling. This document attempts to give a short overview of their properties and definitions for quick reference. But will also provide links to resources that explain the concepts more in depth.

***

## Polynomial interpolation

Polynomial interpolation is the process of fitting a line __through__ a set of control points.

[Jump to full explanation](#Intuition-and-explanation)

---
### Mathematical definition

Let $C$ be a set of $n$ control points, and $K$ be a set of associated knot values. The degree of the polynomial associated to the set $C$ of points is $d=n-1$ and can be calculated through the recursive formula:

$
 p_{i,d}(t)=
    \begin{cases} 
      1 & \text{if }d = 0 \\
      \frac{t_{i+d}-t}{t_{i+d}-t_i}\cdot p_{i,d-1}(t) + \frac{t-t_i}{t_{i+d}-t_i}\cdot p_{i+1,d}(t), & \text{if } d>0 
   \end{cases}
$

Where $t_i \in K$ is a knot value. 

The parametric curve $r$ is evaluated as:

$
 r(t)=
    \begin{cases} 
      c_i & \text{if }d = 0 \\
      \frac{t_{i+d}-t}{t_{i+d}-t_i}\cdot p_{i,d-1}(t) + \frac{t-t_i}{t_{i+d}-t_i}\cdot p_{i+1,d}(t), & \text{if } d>0 
   \end{cases}
$

Where $c_i \in C$ is a control point.

>These polynomials are called _Lagrange Polynomials_. For a more succint definition you can refer to [Wolfram Alpha](http://mathworld.wolfram.com/LagrangeInterpolatingPolynomial.html)

### Algorithm

Although one could simply implement the recursion above, it is far more efficient to use a dynamic programming appproach to compute the parametric curve. The following python implementation shows how to calculate the parametric curve efficiently.

```python
def p_curve(knots, points, t):
    # List lengths must match
    assert(len(s_knots)==len(points))
    point_num = len(points)

    # Initialize value list
    old_list = points.copy()
    # Iterate from lowest polynomial order to highest
    for delta in range (1, point_num):
        # Generate the points of order d from the 
        # points of order d-1
        for ti in range (0, point_num-delta):
            # Calculate current weight for the 
            # left side point
            poly_val1 = (
                (knots[ti+delta]-t)/
                (knots[ti+delta]-knots[ti]))
            # Calculate current weight for the
            # right side point
            poly_val2 = (
                (t-knots[ti])/
                (knots[ti+delta]-knots[ti]))

            # Calculate new point of order d
            # from the 2 points of order d-1 
            old_list[ti]=(
                poly_val1*old_list[ti]+
                poly_val2*old_list[ti+1])

    return old_list[0]
```

We can consider that the original control points are a list of polynomial interpolation poits of order 0 (constant points). From those we calculate all polynomial interpolation points of order 1 (straight lines). From the points of order 1 we calculate those of order 2 (quadratic curves). We proceed like this until we reach the same order as the number of control points. 

Since the number of points is reduced by one with each level, the final computation creates a single point, which is the result of the evaluation of the interpolation curve at parameter $t$ based on the knot vector $K$ and control points matrix $C$. 
___
###Intuition and explanation

___
## References

[Depthful explanation of Interpolating curves. Author unkown.](https://www.uio.no/studier/emner/matnat/ifi/nedlagte-emner/INF-MAT5340/v07/undervisningsmateriale/kap1.pdf)