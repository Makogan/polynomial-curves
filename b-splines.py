from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtCore import pyqtSlot
import math
import sys

class Vec2D(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vec2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vec2D(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        if type(other)==type(self):
            return self.x*other.x + self.y*other.y
        if type(other)==type(1) or type(other)==type(1.0):
            return Vec2D(self.x*other, self.y*other)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __abs__(self):
        return math.sqrt(self.x**2 + self.y**2)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __str__(self):
        return '(%g, %g)' % (self.x, self.y)

    def __ne__(self, other):
        return not self.__eq__(other)  # reuse __eq__

def standardKnot(points, degree):
    order = degree + 1
    m = len(points) - 1
    u=0
    knots = []
    for i in range(0, order):
        knots.append(u)

    for i in range(0, m - order + 1):
        u += 1 / (m - order + 2)
        knots.append(u)

    for i in range(0, order):
        knots.append(1)

    return knots

# Knot vector, control points of the curve, t, spline order
def p_curve(knots, points, t, order=3):
    degree = order - 1

    if len(points) <= degree:
        return points[0]

    # Find the index of the interval containing t
    index = 0
    while not(t>= knots[index] and t < knots[index+1]):
        index += 1

    # Initialize a list with an 'order' number of slots
    control_points = [points[0]] * order
    # Copy the active control points which go from (index-degree) to (index)
    # These are the control points of degree 0
    for i in range(0, order):
        control_points[i] = points[index - degree + i]

    # Generate the intermediary control points, from lowest order to highest
    for l in range(0, degree):
        # Iterate over the control points from back to front (only (degree - current_order)
        # control points are needed to compute the next level)
        for j in range(degree, l, -1):
            alpha = (t - knots[index - degree + j]) / (knots[index - l + j] - knots[index - degree + j])
            control_points[j] = (1.0 - alpha)*control_points[j-1] + alpha * control_points[j]

    return control_points[degree]

class Canvas(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()
        self.points = []
        self.curve = []
        self.knots = []
        self.algo_points=[]

    def initUI(self):
        self.setGeometry(300, 100, 1000, 500)
        self.setWindowTitle('Poly Curves')

    def p_alg(self, knots, points, t):
        #Initialize
        assert(len(knots)==len(points))
        point_num = len(points) - 1
        degree = 2
        order = degree + 1
        if len(points) <= degree:
            return points[0]
        index = 0
        # Extend knot sequence
        k = standardKnot(points, degree)
        #print(k)
        # Find containing interval
        while not(t>= k[index] and t < k[index+1]):
            index += 1
        if index >= point_num + degree:
            print("ERROR")
            index = -1

        old_list = []
        self.algo_points = [points.copy()]
        for i in range(0, order):
            old_list.append(points[index - i])

        self.algo_points.append(old_list.copy())
        level = 1
        for r in range(order, 1, -1):
            i=index
            self.algo_points.append([])
            level += 1
            for s in range(0, r-1):
                omega = (t - k[i]) / (k[i + r -1] - k[i])
                old_list[s] = omega * old_list[s] + (1-omega) * old_list[s+1]
                self.algo_points[level].append(old_list[s])
                print("level " + str(level - 1) + ": " + str(omega))
                print("delta: " + str(r-1))
                print("Interval: " + str(i) + " " + "[" + str(k[i]) + ", " + str(k[i+r-1]) + "]")
                i = i-1
        print('')
        return old_list[0]

    def setKnots(self, knots):
        self.knots = knots

    def setTVal(self, tVal):
        self.tVal = tVal

    def mousePressEvent(self, QMouseEvent):

        self.points.append(Vec2D(QMouseEvent.pos().x(), QMouseEvent.pos().y()))
        knot_list = self.knots.text()
        knot_list = knot_list + str(len(self.points)-1) + ','
        self.knots.setText(knot_list)

        knots = self.knots.text().split(',')
        knots.remove('')

        knot_vector = []
        for p in knots:
            knot_vector.append(float(p))

        self.curve = []
        delta = 0.01
        t=0.0
        last = len(knot_vector)
        order = 4
        knots = standardKnot(self.points, order - 1)
        while t < 1:
            p = p_curve(knots, self.points, t, order)
            t+=delta
            self.curve.append(p)

        self.updateAlg(knot_vector)

        #self.curve.append(self.points[last-1])


    def updateAlg(self, knot_vector):
        t = (self.tVal.value()/100)
        self.slider_header.setText('t = {}'.format(round(t*100)/100.0))
        #self.p_alg(knot_vector, self.points, t)
        self.update()

    def tUpdate(self):
        knots = self.knots.text().split(',')
        knots.remove('')

        knot_vector = []
        for p in knots:
            knot_vector.append(float(p))
        self.updateAlg(knot_vector)


    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawLines(qp)
        qp.end()


    def drawLines(self, qp):

        line_colors = ['#ff0000', '#33ee55', '#ee66ee', '#ffcc22', '#0066aa', '#994400']
        pen = QPen(QColor('#000000'), 1, Qt.SolidLine)
        qp.setPen(pen)
        brush = QBrush(QColor(0,0,255,255))

        max_i = len(self.algo_points)
        for i in range(max_i-2, -1, -1):
            max_j = len(self.algo_points[i])
            color = line_colors[i%(len(line_colors))]
            pen.setColor(QColor(color))
            qp.setPen(pen)
            for j in range(max_j-2, -1, -1):
                p1 = self.algo_points[i][j]
                p2 = self.algo_points[i][j+1]
                p1 = (p1-p2)*100 + p1
                p2 = (p2-p1)*100 + p2
                qp.drawLine(p1.x, p1.y, p2.x, p2.y)
        pen.setColor(QColor(0,0,0,255))
        pen.setWidth(3)
        qp.setPen(pen)
        size = len(self.curve)
        for index in range (1, size) :
            p1 = self.curve[index-1]
            p2 = self.curve[index]
            qp.drawLine(p1.x, p1.y, p2.x, p2.y)

        for i in range (0, max_i):
            vals = self.algo_points[i]
            color = line_colors[i%(len(line_colors))]
            brush.setColor(QColor(color))
            qp.setBrush(brush)
            pen.setColor(QColor(color))
            qp.setPen(pen)
            for p in vals:
                qp.drawEllipse(p.x-5, p.y-5, 10, 10)

    def setSliderHeader(self, s_header):
        self.slider_header = s_header

    def clear(self):
        self.points = []
        self.curve = []
        self.knots.setText('')
        self.update()
        self.algo_points = []

class Frame(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        self.setGeometry(300, 100, 1000, 500)

        pal = QPalette()
        pal.setColor(self.backgroundRole(), QColor(0,59,70))
        self.setAutoFillBackground(True)
        self.setPalette(pal)

        canvas = Canvas()
        pal.setColor(canvas.backgroundRole(), QColor(196,223,230))
        canvas.setAutoFillBackground(True)
        canvas.setPalette(pal)

        knots = QLineEdit()
        knots.setStyleSheet(
            'QLineEdit {'
                'border: none;'
                'color: #000000;'
                'background-color: #aaddff;}'
        )
        knots.setPalette(pal)
        knots.setText('')
        knots.setFixedWidth(120)

        canvas.setKnots(knots)

        knot_header = QLabel('Knot values:')
        knot_header.setFixedWidth(90)
        knot_header.setFixedHeight(30)
        knot_header.setStyleSheet('color: #ffffff')

        button = QPushButton('Clear Knots')
        button.clicked.connect(canvas.clear)
        button.setStyleSheet(
            'QPushButton {'
                'border: none;'
                'color: #ffffff;'
                'background-color: #0066aa;}'
            )
        button.setMinimumHeight(48)

        slider_header = QLabel('t = ')
        slider_header.setFixedWidth(90)
        slider_header.setFixedHeight(30)
        slider_header.setStyleSheet('color: #ffffff')

        canvas.setSliderHeader(slider_header)

        slider = QSlider(Qt.Horizontal)
        slider.setFixedWidth(120)
        slider.setMinimum(0)
        slider.setMaximum(99)
        slider.sliderMoved.connect(canvas.tUpdate)

        canvas.setTVal(slider)

        Layout = QGridLayout()
        Layout.addWidget(knot_header,0,0)
        Layout.addWidget(knots,1,0)
        Layout.addWidget(button, 2,0)
        Layout.addWidget(slider_header,3,0)
        Layout.addWidget(slider, 4,0)
        Layout.addWidget(canvas,0,1,50,50)
        self.setLayout(Layout)

        self.show()

if __name__ == '__main__':

    app = QApplication(sys.argv)
    #app.setStyleSheet(StyleSheet)
    ex = Frame()
    sys.exit(app.exec_())