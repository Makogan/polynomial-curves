import matplotlib.pyplot as plt
import numpy as np
import sympy as sym

def normalize(v):
    norm = np.linalg.norm(v)
    if norm == 0:
       return v
    return v / norm

def GetOrthoNormal(v):
    b1 = np.array([1,0,0])
    dot = np.dot(v, b1)

    if(dot > 0.8):
        return normalize(np.cross(v, np.array([0,1,0])))

    return normalize(np.cross(v, np.array([1,0,0])))

#lobe = np.array([-0.8,-0.8, 1])
lobe = np.array([1,1,1])
lobe = normalize(lobe)

tangent = GetOrthoNormal(lobe)
bi_tangent = normalize(np.cross(tangent, lobe))

basis_m = np.matrix([lobe, tangent, bi_tangent])

x = np.array([[1,0,0]])
y = np.array([[0,1,0]])
z = np.array([[0,0,1]])

mx = (basis_m * x.T * x * basis_m.T)
my = (basis_m * y.T * y * basis_m.T)
mz = (basis_m * z.T * z * basis_m.T)

sym.init_printing(use_unicode=False)
V_x = sym.Symbol('V_x')
V_y = sym.Symbol('V_y')
V_z = sym.Symbol('V_z')

X_x = sym.Symbol('X_x')
X_y = sym.Symbol('X_y')
X_z = sym.Symbol('X_z')

lambda1 = sym.Symbol('\lambda_1')
lambda2 = sym.Symbol('\lambda_2')

V = sym.Matrix([V_x, V_y, V_z])
X = sym.Matrix([X_x, X_y, X_z])

W = sym.Matrix([(X_x ** 2 * lambda1 + V_x ** 2 * lambda2) ** (1.0/2.0),
                (X_y ** 2 * lambda1 + V_y ** 2 * lambda2) ** (1.0/2.0),
                (X_z ** 2 * lambda1 + V_z ** 2 * lambda2) ** (1.0/2.0)])

M2 = V * V.transpose()
M1 = X * X.transpose()
#print(sym.latex(lambda1 * M1 + lambda2 * M2))
print(sym.latex(W * W.transpose()))
#sym.pprint(W * W.transpose())