from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtCore import pyqtSlot
import math
import sys

class Vec2D(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vec2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vec2D(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        if type(other)==type(self):
            return self.x*other.x + self.y*other.y
        if type(other)==type(1) or type(other)==type(1.0):
            return Vec2D(self.x*other, self.y*other)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __abs__(self):
        return math.sqrt(self.x**2 + self.y**2)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __str__(self):
        return '(%g, %g)' % (self.x, self.y)

    def __ne__(self, other):
        return not self.__eq__(other)  # reuse __eq__


def p_curve(knots, points, t):
    # List lengths must match
    assert(len(knots)==len(points))
    point_num = len(points)

    # Initialize value list
    old_list = points.copy()
    # Iterate from lowest polynomial order to highest
    for delta in range (1, point_num):
        # Generate the points of order d from the
        # points of order d-1
        for ti in range (0, point_num-delta):
            # Calculate current weight for the
            # left side point
            poly_val1 = (
                (knots[ti+delta]-t)/
                (knots[ti+delta]-knots[ti]))
            # Calculate current weight for the
            # right side point
            poly_val2 = (
                (t-knots[ti])/
                (knots[ti+delta]-knots[ti]))

            # Calculate new point of order d
            # from the 2 points of order d-1
            old_list[ti]=(
                poly_val1*old_list[ti]+
                poly_val2*old_list[ti+1])

    return old_list[0]

class Canvas(QWidget):

    def __init__(self):
        super().__init__()
        
        self.initUI()
        self.points = []
        self.curve = []
        self.knots = []
        self.algo_points=[]
        
    def initUI(self):
        self.setGeometry(300, 100, 1000, 500)
        self.setWindowTitle('Poly Curves')

    def p_alg(self, knots, points, t):
        # List lengths must match
        assert(len(knots)==len(points))
        point_num = len(points)

        # Initialize value list
        old_list = points.copy()
        self.algo_points = [points.copy()]
        # Iterate from lowest polynomial order to highest
        for delta in range (1, point_num):
            # Generate the points of order d from the
            # points of order d-1
            self.algo_points.append([])
            for ti in range (0, point_num-delta):
                # Calculate current weight for the 
                # left side point
                poly_val1 = (
                    (knots[ti+delta]-t)/
                    (knots[ti+delta]-knots[ti]))
                # Calculate current weight for the
                # right side point
                poly_val2 = (
                    (t-knots[ti])/
                    (knots[ti+delta]-knots[ti]))

                # Calculate new point of order d
                # from the 2 points of order d-1
                p = (
                    poly_val1*old_list[ti]+
                    poly_val2*old_list[ti+1])
                old_list[ti] = p
                self.algo_points[delta].append(p)
        
    def setKnots(self, knots):
        self.knots = knots
    
    def setTVal(self, tVal):
        self.tVal = tVal
    
    def mousePressEvent(self, QMouseEvent):

        self.points.append(Vec2D(QMouseEvent.pos().x(), QMouseEvent.pos().y()))
        knot_list = self.knots.text()
        knot_list = knot_list + str(len(self.points)-1) + ','
        self.knots.setText(knot_list)

        knots = self.knots.text().split(',')
        knots.remove('')

        knot_vector = []
        for p in knots:
            knot_vector.append(float(p))
        
        self.curve = []
        delta = 0.1
        t=0.0
        last = len(knot_vector)
        while t < knot_vector[last-1]:
            p = p_curve(knot_vector, self.points, t)
            t+=delta
            self.curve.append(p)
        
        self.updateAlg(knot_vector)
        
        self.curve.append(self.points[last-1])


    def updateAlg(self, knot_vector):
        t = (self.tVal.value()/100) * knot_vector[len(knot_vector)-1]
        self.slider_header.setText('t = {}'.format(round(t*100)/100.0))
        self.p_alg(knot_vector, self.points, t)
        self.update()
    
    def tUpdate(self):
        knots = self.knots.text().split(',')
        knots.remove('')

        knot_vector = []
        for p in knots:
            knot_vector.append(float(p))
        self.updateAlg(knot_vector)


    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawLines(qp)
        qp.end()
        
    
    def drawLines(self, qp):    

        line_colors = ['#ff0000', '#33ee55', '#ee66ee', '#ffcc22', '#0066aa', '#994400']
        pen = QPen(QColor('#000000'), 1, Qt.SolidLine)
        qp.setPen(pen)
        brush = QBrush(QColor(0,0,255,255))

        max_i = len(self.algo_points)
        for i in range(max_i-2, -1, -1):
            max_j = len(self.algo_points[i])
            color = line_colors[i%(len(line_colors))]
            pen.setColor(QColor(color))
            qp.setPen(pen)
            for j in range(max_j-2, -1, -1):
                p1 = self.algo_points[i][j]
                p2 = self.algo_points[i][j+1]
                p1 = (p1-p2)*100 + p1
                p2 = (p2-p1)*100 + p2
                qp.drawLine(p1.x, p1.y, p2.x, p2.y) 

        pen.setColor(QColor(0,0,0,255))
        pen.setWidth(3)
        qp.setPen(pen)
        size = len(self.curve)
        for index in range (1, size) :
            p1 = self.curve[index-1]
            p2 = self.curve[index]
            qp.drawLine(p1.x, p1.y, p2.x, p2.y) 

        for i in range (0, max_i):
            vals = self.algo_points[i]
            color = line_colors[i%(len(line_colors))]
            brush.setColor(QColor(color))
            qp.setBrush(brush)
            pen.setColor(QColor(color))
            qp.setPen(pen)
            for p in vals:
                qp.drawEllipse(p.x-5, p.y-5, 10, 10)
    
    def setSliderHeader(self, s_header):
        self.slider_header = s_header
    
    def clear(self):
        self.points = []
        self.curve = []
        self.knots.setText('')
        self.update()
        self.algo_points = []

class Frame(QWidget):

    def __init__(self):
        super().__init__()
        
        self.initUI()
        
    def initUI(self):      

        self.setGeometry(300, 100, 1000, 500)

        pal = QPalette()
        pal.setColor(self.backgroundRole(), QColor(0,59,70))
        self.setAutoFillBackground(True)
        self.setPalette(pal)

        canvas = Canvas()
        pal.setColor(canvas.backgroundRole(), QColor(196,223,230))
        canvas.setAutoFillBackground(True)
        canvas.setPalette(pal)

        knots = QLineEdit()
        knots.setStyleSheet(
            'QLineEdit {'
                'border: none;' 
                'color: #000000;' 
                'background-color: #aaddff;}'
        )
        knots.setPalette(pal)
        knots.setText('')
        knots.setFixedWidth(120)

        canvas.setKnots(knots)

        knot_header = QLabel('Knot values:')
        knot_header.setFixedWidth(90)
        knot_header.setFixedHeight(30)
        knot_header.setStyleSheet('color: #ffffff')

        button = QPushButton('Clear Knots')
        button.clicked.connect(canvas.clear)
        button.setStyleSheet(
            'QPushButton {'
                'border: none;' 
                'color: #ffffff;' 
                'background-color: #0066aa;}'
            )
        button.setMinimumHeight(48)

        slider_header = QLabel('t = ')
        slider_header.setFixedWidth(90)
        slider_header.setFixedHeight(30)
        slider_header.setStyleSheet('color: #ffffff')

        canvas.setSliderHeader(slider_header)

        slider = QSlider(Qt.Horizontal)
        slider.setFixedWidth(120)
        slider.setMinimum(0)
        slider.setMaximum(100)
        slider.sliderMoved.connect(canvas.tUpdate)

        canvas.setTVal(slider)

        Layout = QGridLayout()
        Layout.addWidget(knot_header,0,0)
        Layout.addWidget(knots,1,0)
        Layout.addWidget(button, 2,0)
        Layout.addWidget(slider_header,3,0)
        Layout.addWidget(slider, 4,0)
        Layout.addWidget(canvas,0,1,50,50)
        self.setLayout(Layout)

        self.show()
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    #app.setStyleSheet(StyleSheet)
    ex = Frame()
    sys.exit(app.exec_())