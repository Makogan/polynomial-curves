from pyx import *
from pyx.metapost.path import beginknot, endknot, smoothknot, tensioncurve
import os
import math
import scipy as sp
import numpy as np

light_blue = (0, 0.8, 1)
lighter_blue = (0.7, 0.95, 1)

def DrawArrow(p1, p2, tcolor=(0,0,0)):
    c.stroke(path.line(p1[0], p1[1], p2[0], p2[1]),
        [style.linecap.round, color.rgb(tcolor[0], tcolor[1], tcolor[2])])

    p = 0.95 * p1 + 0.05 * p2
    c.stroke(path.line(p1[0], p1[1], p[0], p[1]),
        [
            style.linecap.round,
            trafo.rotate(-30, x=p1[0], y=p1[1]),
            color.rgb(tcolor[0], tcolor[1], tcolor[2])
        ])

def DrawEdge(p1, p2, color1 = (0,0,0), color2 = (0,0,0)):
    dir = (p2 - p1)
    dir = dir / np.linalg.norm(dir)

    c_p1 = p1 + 0.55 * dir
    c_p2 = p2 - 0.55 * dir

    orthogonal_dir = np.array([dir[1], -dir[0]])
    epsilon = 0.23
    DrawArrow(c_p1 + orthogonal_dir * epsilon, c_p2 + orthogonal_dir * epsilon, color1)
    epsilon *= -1
    DrawArrow(c_p2 + orthogonal_dir * epsilon, c_p1 + orthogonal_dir * epsilon, color2)


def DrawFace(p1, p2, p3, fill_color=(1,1,1), outline_color=(0,0,0)):

    f = path.path(path.moveto(p1[0], p1[1]), path.lineto(p2[0], p2[1]),
                  path.lineto(p3[0], p3[1]), path.closepath())

    c.stroke(f,
        [color.rgb.red,
         deco.filled([color.rgb(fill_color[0], fill_color[1], fill_color[2])]),
         deco.stroked([style.linejoin.round,
            color.rgb(outline_color[0], outline_color[1], outline_color[2])]),
         style.linecap.round])


def Normalize(p):
    return p / np.linalg.norm(p)


os.chdir("./HalfEdgeFaceSplit")

unit.set(wscale=5)
c = canvas.canvas()

# Initialize points
l = 15
h = math.sqrt(3)/2.0 * l
p1 = np.array([0,h])
p2 = np.array([-l / 2.0, 0])
p3 = np.array([l / 2.0, 0])

triangle = np.array([[0, h], [-l, 0], [l, 0]])
centroid = triangle.mean(axis=0)

# Draw the edges
DrawEdge(p1, p2, (0,0,0), light_blue)
DrawEdge(p2, p3, (0,0,0), light_blue)
DrawEdge(p3, p1, (0,0,0), light_blue)
# Draw the points
c.fill(path.circle(p1[0], p1[1], 0.15), [color.rgb(light_blue[0], light_blue[1], light_blue[2])])
c.fill(path.circle(p2[0], p2[1], 0.15), [color.rgb(light_blue[0], light_blue[1], light_blue[2])])
c.fill(path.circle(p3[0], p3[1], 0.15), [color.rgb(light_blue[0], light_blue[1], light_blue[2])])
c.fill(path.circle(centroid[0], centroid[1], 0.15))
# Draw the face
affine_scaled = []
pl = [p1, p2, p3]
for i in range(3):
    dir = centroid - pl[i]
    dir = Normalize(dir) * 2.5
    affine_scaled.append(pl[i] + dir)

DrawFace(affine_scaled[0], affine_scaled[1], affine_scaled[2], lighter_blue, light_blue)

for i in range(3):
    m = 0.5 * pl[i] + 0.5 * pl[(i + 1) % 3]
    dir = centroid - m
    dir = dir * 0.8 / np.linalg.norm(dir)
    c.text(m[0] + dir[0], m[1] + dir[1], f"\huge{{$e_{i}$}}", [text.halign.boxcenter])

# Label the points
c.text(p1[0], p1[1] + 0.5, r"\huge{$v_0$}", [text.halign.boxcenter])
c.text(p2[0] - 0.5, p3[1] - 0.5, r"\huge{$v_1$}", [text.halign.boxcenter])
c.text(p3[0] + 0.5, p2[1] - 0.5, r"\huge{$v_2$}", [text.halign.boxcenter])
c.text(centroid[0], centroid[1], r"\huge{$F_0$}", [text.halign.boxcenter])

c.writeEPSfile("face_split_1")
c.writePDFfile("face_split_1")
c.writeSVGfile("face_split_1")
