import math
import os

import numpy as np
import scipy as sp
from pyx import *
from pyx.metapost.path import beginknot, endknot, smoothknot, tensioncurve

light_blue = (0, 0.8, 1)
lighter_blue = (0.7, 0.95, 1)

light_red = (1, 0.3, 0.3)
lighter_red = (1, 0.8, 0.8)

def DrawArrow(p1, p2, tcolor=(0,0,0), label=""):
    p = 0.95 * p1 + 0.05 * p2
    c.stroke(path.line(p1[0], p1[1], p[0], p[1]),
        [
            style.linecap.round,
            trafo.rotate(-30, x=p1[0], y=p1[1]),
            color.rgb(tcolor[0], tcolor[1], tcolor[2])
        ])
    p1, p2 = p2.copy(), p1.copy()
    # Align the text so that it's nto upside down
    normal = (p2 - p1)
    normal = np.array([normal[1], -normal[0]])
    if normal[1] > 0:
        p1, p2 = p2.copy(), p1.copy()
    c.stroke(path.line(p1[0], p1[1], p2[0], p2[1]),
        [
            style.linecap.round, color.rgb(tcolor[0], tcolor[1], tcolor[2]),
            deco.curvedtext(f"\huge{{{label}}}", textattrs=[text.halign.center, color.rgb.black],
            exclude=0.1)
        ])

def DrawEdge(p1, p2, color1 = (0,0,0), color2 = (0,0,0), label1="", label2=""):
    dir = (p2 - p1)
    dir = dir / np.linalg.norm(dir)

    c_p1 = p1 + 0.55 * dir
    c_p2 = p2 - 0.55 * dir

    orthogonal_dir = np.array([dir[1], -dir[0]])
    epsilon = 0.23
    DrawArrow(c_p1 + orthogonal_dir * epsilon, c_p2 + orthogonal_dir * epsilon, color1, label1)
    epsilon *= -1
    DrawArrow(c_p2 + orthogonal_dir * epsilon, c_p1 + orthogonal_dir * epsilon, color2, label2)


def DrawFace(p1, p2, p3, fill_color=(1,1,1), outline_color=(0,0,0)):

    f = path.path(path.moveto(p1[0], p1[1]), path.lineto(p2[0], p2[1]),
                  path.lineto(p3[0], p3[1]), path.closepath())

    c.stroke(f,
        [color.rgb.red,
         deco.filled([color.rgb(fill_color[0], fill_color[1], fill_color[2])]),
         deco.stroked([style.linejoin.round,
            color.rgb(outline_color[0], outline_color[1], outline_color[2])]),
         style.linecap.round])


def Normalize(p):
    return p / np.linalg.norm(p)


def ScaleAffine(p, centroid, coeff):
    dir = centroid - p
    dir = Normalize(dir)
    return p + dir * coeff


def ScaleFace(face, centroid, coeff):
    return (ScaleAffine(face[0], centroid, coeff),
            ScaleAffine(face[1], centroid, coeff),
            ScaleAffine(face[2], centroid, coeff))


os.chdir("./HalfEdgeFaceSplit")

unit.set(wscale=5)
c = canvas.canvas()

# Initialize points
l = 15
h = math.sqrt(3)/2.0 * l
p1 = np.array([0,h])
p2 = np.array([-l / 2.0, 0])
p3 = np.array([l / 2.0, 0])

triangle = np.array([[0, h], [-l, 0], [l, 0]])
centroid = triangle.mean(axis=0)

# Draw the points
c.fill(path.circle(p1[0], p1[1], 0.15), [color.rgb(light_blue[0], light_blue[1], light_blue[2])])
c.fill(path.circle(p2[0], p2[1], 0.15), [color.rgb(light_blue[0], light_blue[1], light_blue[2])])
c.fill(path.circle(p3[0], p3[1], 0.15), [color.rgb(light_blue[0], light_blue[1], light_blue[2])])
c.fill(path.circle(centroid[0], centroid[1], 0.15), [color.rgb(light_red[0], light_red[1], light_red[2])])
# Draw edges
pl = [p1, p2, p3]
labels = [r"$n_{01}$", r"$n_{00}$", r"$n_{10}$", r"$n_{11}$", r"$n_{21}$", r"$n_{20}$"]
for i in range(3):
    dir = centroid - pl[i]
    m = pl[i] + 0.5 * dir
    dir = Normalize(dir) * 2.5
    DrawEdge(centroid, pl[i] + dir * 0.1, light_red, light_red, labels[i * 2], labels[i * 2 + 1])

    e1 = np.copy(pl[i])
    e2 = np.copy(pl[(i + 1) % 3])
    dir =  e2 - e1
    dir = Normalize(dir)
    orthogonal_dir = np.array([-dir[1], dir[0]])
    e1 = e1 - orthogonal_dir * 0.5 + dir * 0.3
    e2 = e2 - orthogonal_dir * 0.5 - dir * 0.3
    DrawEdge(e1, e2, (0,0,0), light_blue)


first = True
for i in range(3):
    e1 = np.copy(pl[i])
    e2 = np.copy(pl[(i + 1) % 3])
    center = (e1 + centroid + e2) / 3.0

    if first:
        c1 = light_blue
        c2 = lighter_blue
        first = False
    else:
        c1 = light_red
        c2 = lighter_red
    DrawFace(
        (e1 - center) * 0.5 + center,
        (e2 - center) * 0.5 + center,
        (centroid - center) * 0.5 + center,
        c2,
        c1)

    c.text(center[0] * 0.96, center[1] * 0.96, f"\huge{{$F_{i}$}}", [text.halign.boxcenter])

for i in range(3):
    m = 0.5 * pl[i] + 0.5 * pl[(i + 1) % 3]
    dir = centroid - m
    dir = dir * 0.25 / np.linalg.norm(dir)
    c.text(m[0] + dir[0], m[1] + dir[1], f"\huge{{$e_{i}$}}", [text.halign.boxcenter])

# Label the points
c.text(p1[0], p1[1] + 0.5, r"\huge{$v_0$}", [text.halign.boxcenter])
c.text(p2[0] - 0.5, p3[1] - 0.5, r"\huge{$v_1$}", [text.halign.boxcenter])
c.text(p3[0] + 0.5, p2[1] - 0.5, r"\huge{$v_2$}", [text.halign.boxcenter])

c.writeEPSfile("face_split_2")
c.writePDFfile("face_split_2")
c.writeSVGfile("face_split_2")
