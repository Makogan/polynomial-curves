import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))

    curve = DrawTriangle((-1, 1), (1, 1), (0, -1))

    arrow = DrawArrow((0.6, 1 + 2 / 5), (0, -1), tcolor=light_green)

    p1, p2 = GetIntersectionPoints(curve, arrow)

    DrawPoint(p1, 0.05, light_red, light_red)

    c.writeEPSfile(os.path.join(file_path, "ray"))
    c.writePDFfile(os.path.join(file_path, "ray"))
    c.writeSVGfile(os.path.join(file_path, "ray"))


if __name__ == "__main__":
    main()
