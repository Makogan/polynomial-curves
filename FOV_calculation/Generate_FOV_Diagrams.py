import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))

    DrawTriangle((-1, 1), (1, 1), (0, -1))

    DrawText((0.5, 1.3), r"\footnotesize{1}")
    DrawText((2.0, 0), r"$\frac{1}{l} = \tan(\theta)$")
    DrawText((1.8, -0.5), r"$\iff l = \frac{1}{\tan(\theta)}$")

    DrawDoubleArrow((0, 1.2), (1, 1.2))

    DrawLine((0, 1), (0, -1), tcolor=light_red, width=0.02)

    DrawPartialCircle((0, -1), (0, -0.6), -29.0, tcolor=light_blue)

    DrawText((0.15, -0.5), r"${\theta}$", tcolor=light_blue)
    DrawText((-0.1, 0), r"${l}$", tcolor=light_red)

    c.writeEPSfile(os.path.join(file_path, "FOV"))
    c.writePDFfile(os.path.join(file_path, "FOV"))
    c.writeSVGfile(os.path.join(file_path, "FOV"))


if __name__ == "__main__":
    main()
