import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))

    curve = DrawTriangle((-1, 1), (1, 1), (0, -1))

    arrow = DrawArrow((0, -1), (0.5, 1), tcolor=light_red)

    DrawText((0.55, 0.4), r"$w$", light_red)

    p1, p2 = GetIntersectionPoints(curve, arrow)

    DrawPoint(p1, 0.05, light_red, light_red)

    DrawArrow((0, 1), p1, light_blue, 0.2)
    DrawArrow((0, -1), (0, 1), light_blue)

    DrawText((0.2, 1.1), r"$u$", light_blue)
    DrawText((-0.1, 0), r"$v$", light_blue)

    c.writeEPSfile(os.path.join(file_path, "ray_properties"))
    c.writePDFfile(os.path.join(file_path, "ray_properties"))
    c.writeSVGfile(os.path.join(file_path, "ray_properties"))


if __name__ == "__main__":
    main()
