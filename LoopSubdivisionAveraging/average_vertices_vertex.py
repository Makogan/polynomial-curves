from pyx import *
from pyx import text
from pyx.metapost.path import beginknot, endknot, smoothknot, tensioncurve
import os
import math
import scipy as sp
import numpy as np
import random

light_blue = (0, 0.8, 1)
lighter_blue = (0.7, 0.95, 1)
pale = (0.9, 0.9, 1)

def DrawArrow(p1, p2, tcolor=(0,0,0)):
    c.stroke(path.line(p1[0], p1[1], p2[0], p2[1]),
        [style.linecap.round, color.rgb(tcolor[0], tcolor[1], tcolor[2])])

    p = 0.95 * p1 + 0.05 * p2
    c.stroke(path.line(p1[0], p1[1], p[0], p[1]),
        [
            style.linecap.round,
            trafo.rotate(-30, x=p1[0], y=p1[1]),
            color.rgb(tcolor[0], tcolor[1], tcolor[2])
        ])

def DrawEdge(p1, p2, color1 = (0,0,0), color2 = (0,0,0)):
    dir = (p2 - p1)
    dir = dir / np.linalg.norm(dir)

    c_p1 = p1 + 0.55 * dir
    c_p2 = p2 - 0.55 * dir

    orthogonal_dir = np.array([dir[1], -dir[0]])
    epsilon = 0.23
    DrawArrow(c_p1 + orthogonal_dir * epsilon, c_p2 + orthogonal_dir * epsilon, color1)
    epsilon *= -1
    DrawArrow(c_p2 + orthogonal_dir * epsilon, c_p1 + orthogonal_dir * epsilon, color2)


def DrawFace(p1, p2, p3, fill_color=(1,1,1), outline_color=(0,0,0)):

    f = path.path(path.moveto(p1[0], p1[1]), path.lineto(p2[0], p2[1]),
                  path.lineto(p3[0], p3[1]), path.closepath())

    c.stroke(f,
        [color.rgb.red,
         deco.filled([color.rgb(fill_color[0], fill_color[1], fill_color[2])]),
         deco.stroked([style.linejoin.round,
            color.rgb(outline_color[0], outline_color[1], outline_color[2])]),
         style.linecap.round])


def Normalize(p):
    return p / np.linalg.norm(p)


def DrawFace(p1, p2, p3, fill_color=(1,1,1), outline_color=(0,0,0)):
    f = path.path(path.moveto(p1[0], p1[1]), path.lineto(p2[0], p2[1]),
                  path.lineto(p3[0], p3[1]), path.closepath())

    c.stroke(f,
        [color.rgb.red,
         deco.filled([color.rgb(fill_color[0], fill_color[1], fill_color[2])]),
         deco.stroked([style.linejoin.round,
            color.rgb(outline_color[0], outline_color[1], outline_color[2])]),
         style.linecap.round])


def DrawLine(points, tcolor=(0,0,0), width=1, stroke=style.linestyle.dashed):
    p1 = points[0]
    p2 = points[1]
    c.stroke(path.line(p1[0], p1[1], p2[0], p2[1]),
        [
            stroke,
            style.linecap.round,
            style.linewidth(width),
            color.rgb(tcolor[0], tcolor[1], tcolor[2])
        ])


def DrawPoint(p1, r, tcolor=(0,0,0)):
    c.fill(path.circle(p1[0], p1[1], r), [color.rgb(tcolor[0], tcolor[1], tcolor[2])])
    c.stroke(path.circle(p1[0], p1[1], r), [color.rgb(0,0,0)])


unit.set(wscale=1)
text.set(text.LatexEngine, texenc='utf-8')
text.preamble(r'\usepackage[utf8]{inputenc}')
text.preamble(r"\usepackage{amssymb}")
text.preamble(r"\usepackage{amsmath}")
text.preamble(r"\usepackage{amsfonts}")
text.preamble(r"\usepackage{graphicx}")

c = canvas.canvas().layer("aa")

file_path = os.path.abspath(os.path.dirname(__file__))

points = [(0,0)]
connections = []
for i in range(7):
    angle = math.pi * 2.0 * (i / 6.0)
    points.append((math.cos(angle), math.sin(angle)))

for i in range(0,7):
    connections += [0, i, (i + 1) if (i + 1) < 7 else 1]

for point in points:
    DrawPoint(point, 0.02)

for i in range(0, len(connections), 3):
    DrawFace(points[connections[i]], points[connections[i+1]], points[connections[i+2]],
        fill_color=pale)

mids = []
for i in range(0, len(connections), 3):
    for j in range(3):
        mid = \
             (np.array(points[connections[ i + j]])
            + np.array(points[connections[i + (j + 1) % 3]])) * 0.5
        mids += [mid]
        DrawPoint(mid, 0.01)

for i in range(0, len(connections), 3):
    for j in range(3):
        DrawLine([mids[i + j], mids[i + (j + 1) % 3]], width=0.01)

DrawPoint((0,0), 0.03, light_blue)

for point in points[1:]:
    c.text(point[0] * 1.2, point[1] * 1.2, r"$u$", [text.halign.boxcenter])

c.text(-1.3, -0.75, r"{$1\text{-}nu$}", [text.halign.boxcenter])

c.writeEPSfile(os.path.join(file_path, "AverageVertex"))
c.writePDFfile(os.path.join(file_path, "AverageVertex"))
c.writeSVGfile(os.path.join(file_path, "AverageVertex"))