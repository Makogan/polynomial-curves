from sympy import *

x, y, z, r1, r2, nx, ny, nz, p0x, p0y, p0z = symbols('x,y,z, R_1, R_2, n_x, n_y, n_z, p_{0z}, p_{1z}, P_{2z}')
c0x, c0y, c0z, c1x, c1y, c1z = symbols('c_{0x}, c_{0y}, c_{0z}, c_{1x}, c_{1y}, c_{1z}')

init_printing()

a = (x - c0x)**2 + (y - c0y)**2 + (z - c0z)**2 - r1**2
d = nx * (x - p0x) + ny * (y - p0y) + nz * (z - p0z)
solve(a, x)

sol1 = solve(a, x)[0]
b = (sol1 - c1x)**2 + (y - c1y)**2 + (z - c1z) ** 2 - r2**2
print(solve(b, y))