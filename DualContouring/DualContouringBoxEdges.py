import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def DrawBox(points, styles=[SOLID_LINE] * 12, tcolor=(0, 0, 0)):
    width = 0.02

    offset = np.array([-0.3, 0.3])
    bpoints = points + offset
    DrawLine(bpoints[0], bpoints[2], width=width, stroke=styles[5], tcolor=light_blue)
    DrawLine(bpoints[0], bpoints[1], width=width, stroke=styles[4], tcolor=light_red)
    DrawLine(bpoints[1], bpoints[3], width=width, stroke=styles[6], tcolor=light_blue)
    DrawLine(bpoints[2], bpoints[3], width=width, stroke=styles[7], tcolor=light_red)

    DrawLine(points[0], bpoints[0], width=width, stroke=styles[8], tcolor=light_green)
    DrawLine(points[1], bpoints[1], width=width, stroke=styles[9], tcolor=light_green)
    DrawLine(points[2], bpoints[2], width=width, stroke=styles[10], tcolor=light_green)
    DrawLine(points[3], bpoints[3], width=width, stroke=styles[11], tcolor=light_green)

    DrawLine(points[0], points[1], width=width, stroke=styles[0], tcolor=light_red)
    DrawLine(points[2], points[3], width=width, stroke=styles[3], tcolor=light_red)
    DrawLine(points[0], points[2], width=width, stroke=styles[1], tcolor=light_blue)
    DrawLine(points[1], points[3], width=width, stroke=styles[2], tcolor=light_blue)


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))

    points = np.array([(0, 0), (0, 1), (1, 0), (1, 1)])

    DrawBox(points)
    c.writeSVGfile(os.path.join(file_path, "BoxEdges"))


if __name__ == "__main__":
    main()
