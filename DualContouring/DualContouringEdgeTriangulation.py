import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))

    for i in range(2):
        for j in range(2):
            DrawLine((i, j), (i + 1, j), stroke=SOLID_LINE, width=0.02)
            DrawLine((i, j), (i, j + 1), stroke=SOLID_LINE, width=0.02)
            DrawLine((i + 1, j), (i + 1, j + 1), stroke=SOLID_LINE, width=0.02)
            DrawLine((i, j + 1), (i + 1, j + 1), stroke=SOLID_LINE, width=0.02)

    DrawPoint((0, 0), 0.05, tcolor=light_blue)
    DrawPoint((0, 1), 0.05, tcolor=light_blue)
    DrawPoint((0, 2), 0.05, tcolor=light_blue)
    DrawPoint((1, 0), 0.05, tcolor=light_blue)
    DrawPoint((1, 1), 0.05, tcolor=light_blue)
    DrawPoint((1, 2), 0.05, tcolor=light_red)
    DrawPoint((2, 0), 0.05, tcolor=light_blue)
    DrawPoint((2, 1), 0.05, tcolor=light_red)
    DrawPoint((2, 2), 0.05, tcolor=light_red)

    points = [(0.1, 2.2), (0.3, 1.7), (1.5, 1.5), (1.7, 0.7), (2.5, 0.7)]
    DrawCurve(points)

    c.writeSVGfile(os.path.join(file_path, "TopView"))


if __name__ == "__main__":
    main()
