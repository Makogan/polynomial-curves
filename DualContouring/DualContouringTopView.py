import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))
    offset = np.array([0.5, 0.5])

    points = [(0, 0), (0, 1), (1, 0), (1, 1)]

    DrawTriangle(
        points[0] + offset,
        points[1] + offset,
        points[3] + offset,
        outline_color=light_blue,
        fill_color=lighter_blue,
    )
    DrawTriangle(
        points[0] + offset,
        points[2] + offset,
        points[3] + offset,
        outline_color=light_blue,
        fill_color=lighter_blue,
    )

    for i in range(2):
        for j in range(2):
            DrawLine((i, j), (i + 1, j), stroke=SOLID_LINE, width=0.02)
            DrawLine((i, j), (i, j + 1), stroke=SOLID_LINE, width=0.02)
            DrawLine((i + 1, j), (i + 1, j + 1), stroke=SOLID_LINE, width=0.02)
            DrawLine((i, j + 1), (i + 1, j + 1), stroke=SOLID_LINE, width=0.02)

    DrawPoint((1, 1), 0.05, tcolor=light_green)

    DrawPoint((0.5, 0.5), 0.05, tcolor=light_blue)
    DrawPoint((0.5, 1.5), 0.05, tcolor=light_blue)
    DrawPoint((1.5, 0.5), 0.05, tcolor=light_blue)
    DrawPoint((1.5, 1.5), 0.05, tcolor=light_blue)

    c.writeSVGfile(os.path.join(file_path, "EdgeTriangulation"))


if __name__ == "__main__":
    main()
