import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))

    points = np.array([(0, 0), (0, 1), (1, 0), (1, 1)])

    Draw3DGrid((3, 3, 3), highlight=False, highlight_coords=(1, 1, 1))

    DrawDoubleArrow((-0.7, 0.3), (0.3, -0.7), tcolor=light_blue)

    offset = 0.9
    interim = 0.13333
    DrawLine((0.7, -0.8), (0.7 + offset, -0.8), stroke=SOLID_LINE, tcolor=light_green)
    DrawLine(
        (0.7 + offset + interim, -0.8),
        (0.7 + interim + offset * 2, -0.8),
        stroke=SOLID_LINE,
        tcolor=light_green,
    )
    DrawLine(
        (0.7 + (offset + interim) * 2, -0.8),
        (0.7 + (offset + interim) * 2 + offset, -0.8),
        stroke=SOLID_LINE,
        tcolor=light_green,
    )

    DrawText((-0.3, -0.4), r"$s$", tcolor=light_blue)

    DrawText((2.2, -1.1), r"$n$", tcolor=light_green)

    c.writeSVGfile(os.path.join(file_path, "DCGrid"))


if __name__ == "__main__":
    main()
