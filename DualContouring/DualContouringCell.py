import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))

    points = np.array([(0, 0), (0, 1), (1, 0), (1, 1)])

    Draw3DGrid((3, 3, 3), highlight=True, highlight_coords=(1, 1, 1))

    c.writeSVGfile(os.path.join(file_path, "CellGrid"))


if __name__ == "__main__":
    main()
