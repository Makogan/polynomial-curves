import scipy as sp
import random

import os, sys

array = os.path.abspath(__file__).split("/")
parent = "/".join(array[0:-2])
sys.path.append(parent)
from Shared.diagrams import *


def DrawMockSphere(layer):
    if layer == 1:
        DrawCircle((1.8, 1.2), 1.5, tcolor=light_blue + (0.5,))


def main():
    file_path = os.path.abspath(os.path.dirname(__file__))

    points = np.array([(0, 0), (0, 1), (1, 0), (1, 1)])

    Draw3DGrid(
        (3, 3, 3),
        highlight=False,
        highlight_coords=(1, 1, 1),
        draw_at_layer=DrawMockSphere,
    )

    c.writeSVGfile(os.path.join(file_path, "SphereGrid"))


if __name__ == "__main__":
    main()
