import matplotlib.pyplot as plt

def initKnots(start, end, knot_num):
    length = end-start
    delta = length/(knot_num-1)

    knots = []
    while(start <= end):
        knots.append(start)
        start += delta
    return knots

def lagrangeBasis(knots, t, j):
    poly_val = 1.0
    for i in range (0, len(knots)):
        if i!=j:
            poly_val*=(t-knots[i]) / (knots[j] - knots[i])
    return poly_val

def main():

    samples = []
    parameters = []
    degree = 3
    for d in range (0, degree):
        knots = initKnots(-1, 1, d+2)
        samples.append([])
        parameters.append([])
        for i in range(0,len(knots)):
            samples[d].append([])
            parameters[d].append([])
            t = knots[0]
            while t < knots[len(knots)-1] + 0.01:
                samples[d][i].append(lagrangeBasis(knots, t, i))
                parameters[d][i].append(t)
                t += 0.01

    plt.ylim(-0.5, 1.25)
    plt.xlim(-1, 1)
    plt.grid(color=(0.5,0.5,0.5), linestyle=':', linewidth=0.5)

    ax = plt.subplot(111)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    ax.tick_params(axis=u'both', which=u'both', direction='in')

    for d in range(0,degree):
        for i in range (0, len(parameters[d])):
            if(i==0):
                plt.plot(parameters[d][i], samples[d][i], color='C' + str(d), label='Order ' + str(d))
            else:
                plt.plot(parameters[d][i], samples[d][i], color='C' + str(d))

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05), ncol=3, fancybox=True, shadow=True)
    plt.show()

if __name__ == '__main__':
    main()