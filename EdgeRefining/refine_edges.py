from pyx import *
from pyx.metapost.path import beginknot, endknot, smoothknot, tensioncurve
import os
import math
import scipy as sp
import numpy as np
import random

light_blue = (0, 0.8, 1)
lighter_blue = (0.7, 0.95, 1)

def DrawArrow(p1, p2, tcolor=(0,0,0)):
    c.stroke(path.line(p1[0], p1[1], p2[0], p2[1]),
        [style.linecap.round, color.rgb(tcolor[0], tcolor[1], tcolor[2])])

    p = 0.95 * p1 + 0.05 * p2
    c.stroke(path.line(p1[0], p1[1], p[0], p[1]),
        [
            style.linecap.round,
            trafo.rotate(-30, x=p1[0], y=p1[1]),
            color.rgb(tcolor[0], tcolor[1], tcolor[2])
        ])

def DrawEdge(p1, p2, color1 = (0,0,0), color2 = (0,0,0)):
    dir = (p2 - p1)
    dir = dir / np.linalg.norm(dir)

    c_p1 = p1 + 0.55 * dir
    c_p2 = p2 - 0.55 * dir

    orthogonal_dir = np.array([dir[1], -dir[0]])
    epsilon = 0.23
    DrawArrow(c_p1 + orthogonal_dir * epsilon, c_p2 + orthogonal_dir * epsilon, color1)
    epsilon *= -1
    DrawArrow(c_p2 + orthogonal_dir * epsilon, c_p1 + orthogonal_dir * epsilon, color2)


def DrawFace(p1, p2, p3, fill_color=(1,1,1), outline_color=(0,0,0)):

    f = path.path(path.moveto(p1[0], p1[1]), path.lineto(p2[0], p2[1]),
                  path.lineto(p3[0], p3[1]), path.closepath())

    c.stroke(f,
        [color.rgb.red,
         deco.filled([color.rgb(fill_color[0], fill_color[1], fill_color[2])]),
         deco.stroked([style.linejoin.round,
            color.rgb(outline_color[0], outline_color[1], outline_color[2])]),
         style.linecap.round])


def Normalize(p):
    return p / np.linalg.norm(p)


def DrawTriangle(points, tcolor=(0,0,0)):
    for i in range(0, len(points)):
        p1 = points[i]
        p2 = points[(i + 1) % len(points)]
        c.stroke(path.line(p1[0], p1[1], p2[0], p2[1]),
            [
                style.linecap.round,
                color.rgb(tcolor[0], tcolor[1], tcolor[2])
            ])


def DrawLine(points, tcolor=(0,0,0), width=1):
    p1 = points[0]
    p2 = points[1]
    c.stroke(path.line(p1[0], p1[1], p2[0], p2[1]),
        [
            style.linecap.round,
            style.linewidth(width),
            color.rgb(tcolor[0], tcolor[1], tcolor[2])
        ])


def DrawPoint(p1, r, tcolor=(0,0,0)):
    c.fill(path.circle(p1[0], p1[1], r), [color.rgb(tcolor[0], tcolor[1], tcolor[2])])
    c.stroke(path.circle(p1[0], p1[1], r), [color.rgb(0,0,0)])


def DrawConnections():
    DrawLine([vs[0], vs[34]], width=0.015, tcolor=light_blue)
    DrawLine([vs[0], vs[31]], width=0.015, tcolor=light_blue)
    DrawLine([vs[0], vs[ 8]], width=0.015, tcolor=light_blue)

    DrawLine([vs[0], vs[ 3]], width=0.015, tcolor=light_blue)
    DrawLine([vs[0], vs[ 1]], width=0.015, tcolor=light_blue)
    DrawLine([vs[0], vs[ 2]], width=0.015, tcolor=light_blue)

    DrawLine([vs[8], vs[36]], width=0.015, tcolor=light_blue)
    DrawLine([vs[8], vs[ 7]], width=0.015, tcolor=light_blue)
    DrawLine([vs[8], vs[ 6]], width=0.015, tcolor=light_blue)

    DrawLine([vs[6], vs[32]], width=0.015, tcolor=light_blue)
    DrawLine([vs[6], vs[10]], width=0.015, tcolor=light_blue)
    DrawLine([vs[6], vs[ 9]], width=0.015, tcolor=light_blue)

    DrawLine([vs[10], vs[37]], width=0.015, tcolor=light_blue)
    DrawLine([vs[10], vs[27]], width=0.015, tcolor=light_blue)
    DrawLine([vs[10], vs[12]], width=0.015, tcolor=light_blue)

    DrawLine([vs[27], vs[28]], width=0.015, tcolor=light_blue)
    DrawLine([vs[27], vs[40]], width=0.015, tcolor=light_blue)
    DrawLine([vs[27], vs[25]], width=0.015, tcolor=light_blue)

    DrawLine([vs[25], vs[22]], width=0.015, tcolor=light_blue)
    DrawLine([vs[25], vs[39]], width=0.015, tcolor=light_blue)
    DrawLine([vs[25], vs[24]], width=0.015, tcolor=light_blue)

    DrawLine([vs[22], vs[23]], width=0.015, tcolor=light_blue)
    DrawLine([vs[22], vs[35]], width=0.015, tcolor=light_blue)
    DrawLine([vs[22], vs[21]], width=0.015, tcolor=light_blue)

    DrawLine([vs[23], vs[18]], width=0.015, tcolor=light_blue)
    DrawLine([vs[23], vs[20]], width=0.015, tcolor=light_blue)
    DrawLine([vs[23], vs[38]], width=0.015, tcolor=light_blue)

    DrawLine([vs[20], vs[17]], width=0.015, tcolor=light_blue)
    DrawLine([vs[20], vs[34]], width=0.015, tcolor=light_blue)
    DrawLine([vs[20], vs[15]], width=0.015, tcolor=light_blue)


def DrawWireframe():
    for face in connections:
        DrawTriangle([points[face[0]], points[face[1]], points[face[2]]])
    for face in connections:
        for i in range(0, 3):
            DrawPoint(points[face[i]], 0.02)


unit.set(wscale=1)

c = canvas.canvas().layer("aa")

width = 4
height = 3
points = [None] * width * height
random.seed(79)
for i in range(0, height):
    for j in range(0, width):
        v1 = random.uniform(-0.1, 0.1)
        v2 = random.uniform(-0.3, 0.3)

        sign = -((i - 2) % 2)
        points[i * width + j] = [j + sign * (i % 2) * 0.5 + v1 - i * 0.3, i + v2]
        points[i * width + j][0] *= (1 / (i * 0.2 + 1))
        points[i * width + j][1] *= (1 / (i * 0.2 + 1))

connections = []
for i in range(0, height - 1):
    for j in range(0, width - 1):

        sign = -((i - 2) % 2)
        i1 = i * width + j
        i2 = i * width + j + 1
        i3 = (i + 1) * width + j + 1
        i4 = (i + 1) * width + j

        p1 = points[i1]
        p2 = points[i2]
        p3 = points[i3]
        p4 = points[i4]

        if i % 2 == 0:
            connections.append([i1, i3, i4])
            if j < width - 2:
                connections.append([i1, i2, i3])

        else:
            connections.append([i1, i2, i4])
            if j < width - 2:
                connections.append([i4, i3, i2])

DrawWireframe()

file_path = os.path.abspath(os.path.dirname(__file__))
c.writeEPSfile(os.path.join(file_path, "edge_refinment_start"))
c.writePDFfile(os.path.join(file_path, "edge_refinment_start"))
c.writeSVGfile(os.path.join(file_path, "edge_refinment_start"))

c = canvas.canvas().layer("bb")
mids = []
for face in connections:
    for i in range(0, 3):
        mid = (np.array(points[face[i]]) + np.array(points[face[(i + 1) % 3]])) * 0.5
        mids.append(mid)

vs = mids + points
DrawConnections()

DrawWireframe()

for mid in mids:
    DrawPoint(mid, 0.04, light_blue)

c.writeEPSfile(os.path.join(file_path, "edge_refinment_split"))
c.writePDFfile(os.path.join(file_path, "edge_refinment_split"))
c.writeSVGfile(os.path.join(file_path, "edge_refinment_split"))

c = canvas.canvas().layer("cc")

for i in range(0, len(mids), 3):
    DrawLine([mids[i], mids[i+1]], width=0.015, tcolor=light_blue)
    DrawLine([mids[i+1], mids[i+2]], width=0.015, tcolor=light_blue)
    DrawLine([mids[i+2], mids[i]], width=0.015, tcolor=light_blue)

DrawWireframe()
for mid in mids:
    DrawPoint(mid, 0.04, light_blue)

c.writeEPSfile(os.path.join(file_path, "edge_refinment_flip"))
c.writePDFfile(os.path.join(file_path, "edge_refinment_flip"))
c.writeSVGfile(os.path.join(file_path, "edge_refinment_flip"))